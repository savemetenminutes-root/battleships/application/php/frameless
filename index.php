<?php

declare(strict_types=1);

// Delegate static file requests back to the webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

session_start();

// Hide the global scope
echo (function () {
    $response         = null;
    $configAggregator = new \Smtm\Frameless\Config\ConfigAggregator();
    $config           = $configAggregator->aggregate(__DIR__ . '/config/config.php');
    $request          =
        [
            'uri'     => parse_url($_SERVER['REQUEST_URI']),
            'method'  => strtolower($_SERVER['REQUEST_METHOD']),
            'request' => $_REQUEST,
        ];

    $response = null;
    try {
        $currentRoute = null;
        foreach ($config['routes'] as $route) {
            if ($request['uri']['path'] === $route['path'] && $request['method'] === strtolower($route['method'])) {
                $currentRoute = $route;
                break;
            }
        }
        if ($currentRoute === null) {
            $controllerFactory = $config['dependencies'][\Smtm\Battleships\Controller\ErrorController::class];
            $controllerFactory = new $controllerFactory();
            $controller        = $controllerFactory($config, $request);
            $response          = $controller->notFound();
        } else {
            $controllerFactory = $config['dependencies'][$currentRoute['action']['controller']];
            $controllerFactory = new $controllerFactory();
            $controller        = $controllerFactory($config, $request);
            $response          = $controller->{$currentRoute['action']['method']}();
        }
    } catch (\Throwable $e) {
        $response = $e;
    }
    if ($response instanceof \Throwable || !$response instanceof \Smtm\Frameless\View\ViewInterface) {
        return $response;
    }
    return $response->render();
})();
